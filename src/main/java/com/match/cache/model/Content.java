package com.match.cache.model;

import java.util.Date;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;

/**
 * This class represents the model that will be/is already saved in the cache.
 */
public class Content {
	private Date cachedDate;
	private String content;
	private int timeToLive;
	private boolean fromCache;
	private HttpHeaders responseHeaders;
	private HttpStatus responseStatus;
	
	public Date getCachedDate() {
		return cachedDate;
	}

	public void setCachedDate(Date cachedDate) {
		this.cachedDate = cachedDate;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getTimeToLive() {
		return timeToLive;
	}

	public void setTimeToLive(int timeToLive) {
		this.timeToLive = timeToLive;
	}

	public boolean isFromCache() {
		return fromCache;
	}

	public void setFromCache(boolean fromCache) {
		this.fromCache = fromCache;
	}

	public HttpHeaders getResponseHeaders() {
		return responseHeaders;
	}

	public void setResponseHeaders(HttpHeaders responseHeaders) {
		this.responseHeaders = responseHeaders;
	}

	public HttpStatus getResponseStatus() {
		return responseStatus;
	}

	public void setResponseStatus(HttpStatus responseStatus) {
		this.responseStatus = responseStatus;
	}
}
