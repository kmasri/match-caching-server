package com.match.cache.server.rest;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;

import com.match.cache.context.CacheContext;
import com.match.cache.model.Content;
import com.match.cache.server.service.ContentHelper;

/**
 * This class identifies an end point to get the requested resource.
 * Loads system conf time to live.
 */
@RestController
public class CachingProxyController {
	@Resource
	private ContentHelper contentHelper;
	@Resource
	private CacheContext cacheContext;

	@Value("${match.content.timeToLive}")
	private int systemTimeToLive;

	/**
	 * This method will try to server the requested resource.
	 * If the request contains a ttl, this ttl will override the one configured for this application.
	 * @param timeToLive
	 * @param resource, requested resource which is a valid url. 
	 * @return requested resource
	 * @throws RestClientException
	 */
	@RequestMapping(value = "/resource", method = RequestMethod.GET)
	public ResponseEntity<String> requestResource(
			@RequestHeader(name = ContentHelper.CACHE_TTL, required = false) Integer timeToLive,
			@RequestParam("url") String resource) throws RestClientException {
		
		timeToLive = timeToLive == null || timeToLive <= 0 ? systemTimeToLive : timeToLive;
		Content content = cacheContext.getContent(resource, timeToLive);
		return new ResponseEntity<String>(content.getContent(), contentHelper.buildResponseHeaders(content),
				content.getResponseStatus());
	}
}
