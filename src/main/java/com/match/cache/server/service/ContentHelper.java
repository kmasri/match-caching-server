package com.match.cache.server.service;

import java.util.Date;
import java.util.StringJoiner;

import javax.annotation.Resource;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.match.cache.model.Content;

/**
 * It gets the resource and creates cached content.
 */
@Component
public class ContentHelper {
	public static final String CACHE_HIT = "X-CACHE-HIT";
	public static final String CACHE_MISS = "X-CACHE-MISS";
	public static final String CACHE_AGE = "X-CACHE-AGE";
	public static final String CACHE_TTL = "X-CACHE-SET-TTL";

	@Resource
	private RestTemplate restTemplate;

	/**
	 * It gets resource from the actual server via rest template.
	 * @param resource
	 * @param timeToLive
	 * @return
	 */
	public Content getContentFromServer(String resource, int timeToLive) {
		Content result;
		ResponseEntity<String> response = restTemplate.postForEntity(resource, HttpMethod.GET, null, String.class);
		String contentStr = response.getBody();
		HttpHeaders headers = response.getHeaders();
		HttpStatus status = response.getStatusCode();
		
		result = new Content();
		result.setContent(contentStr);
		result.setFromCache(false);
		result.setTimeToLive(timeToLive);
		result.setCachedDate(new Date());
		result.setResponseHeaders(headers);
		result.setResponseStatus(status);
		return result;
	}

	/**
	 * It adds: 
	 * 1. customized headers to the response
	 * 	a. age of the cached content.
	 * 	b. cache hit or miss in case content is not served from cache. 
	 * 2. headers that come from original server's response.
	 * @param content
	 * @return
	 */
	public HttpHeaders buildResponseHeaders(Content content) {
		HttpHeaders result = new HttpHeaders();
		String cacheStatusHeader = content != null && content.isFromCache() ? CACHE_HIT : CACHE_MISS;
		result.set(cacheStatusHeader, "");
		result.set(CACHE_AGE, String.valueOf((new Date().getTime() - content.getCachedDate().getTime()) / 1000));
		for (String key : content.getResponseHeaders().keySet()) {
			StringJoiner stringJoiner = new StringJoiner(",");
			content.getResponseHeaders().get(key).forEach(e -> stringJoiner.add(e));
			result.set(key, stringJoiner.toString());
		}
		return result;

	}
}
