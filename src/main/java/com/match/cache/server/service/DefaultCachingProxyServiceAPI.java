package com.match.cache.server.service;

import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.stereotype.Service;

import com.match.cache.model.Content;
import com.match.cache.server.api.CachingProxyServiceAPI;
import com.match.cache.server.exception.ResourceNotFoundException;

/**
 * This class represents the cache service.
 */
@Service
public class DefaultCachingProxyServiceAPI implements CachingProxyServiceAPI {
	private Map<String, Content> cache = new ConcurrentHashMap<>();

	/*
	 * It looks for the resource in the cache.
	 */
	@Override
	public Content findInCache(String resource) {
		Content result = cache.get(resource);
		if (result != null) {
			return result;
		}
		throw new ResourceNotFoundException();
	}

	/*
	 * It adds resource, its related content to the cache and removes later when its tll expires.
	 */
	@Override
	public void addToCache(String resource, Content content) {
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				cache.remove(resource);
			}
		}, content.getTimeToLive() * 1000);
		cache.put(resource, content);
	}

	/*
	 * It enforces clearing the cache.
	 */
	@Override
	public void evictCache() {
		cache.clear();
	}
}
