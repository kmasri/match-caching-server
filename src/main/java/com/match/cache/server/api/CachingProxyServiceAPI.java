package com.match.cache.server.api;

import com.match.cache.model.Content;
import com.match.cache.server.exception.ResourceNotFoundException;

/**
 * Defines the contract of the cache proxy service.
 */
public interface CachingProxyServiceAPI {
	
	Content findInCache(String resource) throws ResourceNotFoundException;
	void addToCache(String resource, Content content);
	void evictCache();
}
