package com.match.cache.context;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.match.cache.model.Content;
import com.match.cache.server.api.CachingProxyServiceAPI;
import com.match.cache.server.exception.ResourceNotFoundException;
import com.match.cache.server.service.ContentHelper;

/**
 * This class decides to get the resource either from the cache or the actual server.  
 */
@Component
public class CacheContext {
	private static final Logger logger = LoggerFactory.getLogger(CacheContext.class);
	@Resource
	private ContentHelper contentHelper;

	@Resource
	private CachingProxyServiceAPI cachingProxyServiceAPI;

	public Content getContent(String resource, int timeToLive) {
		Content result = null;
		try {
			result = cachingProxyServiceAPI.findInCache(resource);
			result.setFromCache(true);
			logger.debug("Resource " + resource + " served from cache");
			 
		} catch (ResourceNotFoundException e) {
			result = contentHelper.getContentFromServer(resource, timeToLive);
			cachingProxyServiceAPI.addToCache(resource, result);
			logger.debug("Resource " + resource + " served from server");
		}
		return result;
	}
}
