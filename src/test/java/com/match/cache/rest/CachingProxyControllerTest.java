package com.match.cache.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.annotation.Resource;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.match.cache.server.api.CachingProxyServiceAPI;
import com.match.cache.server.service.ContentHelper;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CachingProxyControllerTest {
	public static final String PATH_ROOT = "/resource";
	public static final String URL_GOOGLE = "http://www.google.com";
	public static final String PATH_RESOURCE_GOOGLE = PATH_ROOT + "?url=" + URL_GOOGLE;

	@Resource
	private WebApplicationContext context;

	@Resource
	private CachingProxyServiceAPI cachingProxyServiceAPI;

	private MockMvc mvc;

	@Before
	public void setup() {
		mvc = MockMvcBuilders.webAppContextSetup(context).build();
	}

	@After
	public void teardown() {
		cachingProxyServiceAPI.evictCache();
	}

	@Test
	public void testRequestResource() throws Exception {
		mvc.perform(get(PATH_RESOURCE_GOOGLE)).andExpect(status().isOk())
				.andExpect(header().string(ContentHelper.CACHE_MISS, ""));
	}

	@Test
	public void testRequestResourceTwice() throws Exception {
		mvc.perform(get(PATH_RESOURCE_GOOGLE)).andExpect(status().isOk())
				.andExpect(header().string(ContentHelper.CACHE_MISS, ""));
		mvc.perform(get(PATH_RESOURCE_GOOGLE)).andExpect(status().isOk())
				.andExpect(header().string(ContentHelper.CACHE_HIT, ""));
	}
}
