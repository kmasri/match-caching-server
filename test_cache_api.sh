#!/bin/bash
mvn spring-boot:run &
sleep 10
url=$1
#expected X-CACHE-MISS
curl -s -I --header "X-CACHE-SET-TTL:10" "http://localhost:8080/resource?url=$url"
#expected X-CACHE-HIT
curl -s -I --header "X-CACHE-SET-TTL:10" "http://localhost:8080/resource?url=$url"

sleep 11
#expected X-CACHE-MISS
curl -s -I --header "X-CACHE-SET-TTL:10" "http://localhost:8080/resource?url=$url"

